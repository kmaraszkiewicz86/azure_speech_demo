﻿

using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;
using Microsoft.CognitiveServices.Speech.Translation;

var key = "******";
var region = "***";

while(true)
{
    LanguageSettings en = new("en", "en-US", "en-US-EricNeural");
    LanguageSettings pl = new("pl", "pl-PL", "pl-PL-MarekNeural");

    Console.WriteLine(pl);
    Console.ReadLine();

    await Translate(pl.Locale, en);

    Console.WriteLine(en);
    Console.ReadLine();
    await Translate(en.Locale, pl);
}

async Task Translate(string sourceLocale, LanguageSettings languageSettings)
{
    var translationConfig = SpeechTranslationConfig.FromSubscription(key, region);
    translationConfig.SpeechRecognitionLanguage = sourceLocale;
    translationConfig.AddTargetLanguage(languageSettings.Language);

    using var audioConfig = AudioConfig.FromDefaultMicrophoneInput();

    using var recognizer = new TranslationRecognizer(translationConfig, audioConfig);
    var result = await recognizer.RecognizeOnceAsync();

    Console.WriteLine($"Input: {result.Text}");
    var translatedResult = result.Translations[languageSettings.Language];
    Console.WriteLine($"Output: {translatedResult}");

    var config = SpeechConfig.FromSubscription(key, region);
    config.SpeechRecognitionLanguage = languageSettings.Locale;
    config.SpeechSynthesisVoiceName = languageSettings.VoiceName;

    using var s = new SpeechSynthesizer(config);
    await s.SpeakTextAsync(translatedResult);
}

record LanguageSettings(string Language, string Locale, string VoiceName);